# Argo CD Demo
This demo is inspired by the Getting started guide from Argo CD: https://argo-cd.readthedocs.io/en/stable/getting_started/
# Requirements

Beside a Kubernetes cluster following tools are required to run the demo:
- kubectl (https://kubernetes.io/docs/tasks/tools/#kubectl)
- argocd (https://argo-cd.readthedocs.io/en/stable/cli_installation/#homebrew)

## Setup Argo CD

First Connect to the cluster you want to deploy Argo CD. Argo CD can the be installed in the cluster
```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

## Access Argo CD UI

To use the UI you need to fetch the initial admin password and login to the dashboard.

```bash
argocd admin initial-password -n argocd
kubectl port-forward svc/argocd-server -n argocd 8080:443
```

The dashboard is now accessable via http://localhost:8080. For the authentification it uses your cluster role.

## Setup your first Application

Before setting up the first application it is recommanded to install a project where the application is installed, otherwise the application will be installed in the `default`` project.

```bash
kubectl apply -n argocd -f argocd/templates/project.yaml
```

To sync resources between a git repository and a cluster an application need to be defined. Key aspects of an application is the source of the repo and the target cluster ArgoCD should use for the sync.

```bash
kubectl apply -n argocd -f argocd/templates/app.yaml
```

ArgoCD does now track the changes within the git repository and apply changes for each new commit.
